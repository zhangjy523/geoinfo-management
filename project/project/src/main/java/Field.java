
import java.util.ArrayList;

import org.locationtech.jts.geom.Polygon;

public class Field {
	private int id;
	private String field_id;
	private Polygon ply;
	private Species specie;
	private String soil;
	private ArrayList<Treat> treatment;
	

	public Field(int id) {
		// TODO Auto-generated constructor stub
		this.id = id;
		
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getField_id() {
		return field_id;
	}


	public void setField_id(String field_id) {
		this.field_id = field_id;
	}


	public Polygon getPly() {
		return ply;
	}


	public void setPly(Polygon ply) {
		this.ply = ply;
	}


	public Species getSpecie() {
		return specie;
	}


	public void setSpecie(Species specie) {
		this.specie = specie;
	}


	public String getSoil() {
		return soil;
	}


	public void setSoil(String soil) {
		this.soil = soil;
	}


	public ArrayList<Treat> getTreatment() {
		return treatment;
	}


	public void setTreatment(ArrayList<Treat> treatment) {
		this.treatment = treatment;
	}

	
	

}
