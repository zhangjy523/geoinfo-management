
import java.nio.charset.Charset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * 
 */

/**
 * @author victor
 *
 */
public class WeatherInfo {
	
	public static String retrieveWeatherForecastAsCsv() throws Exception {
		//set up the end point

		URIBuilder builder = new URIBuilder("https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/weatherdata/forecast");
		builder.setParameter("aggregateHours", "24")
			.setParameter("contentType", "csv")
			.setParameter("unitGroup", "metric")
			.setParameter("locationMode", "single")
			.setParameter("key", "BUFFZ493AMSLG33VHFPQ7QRPF")
			.setParameter("locations", "Rheinbach, Nordrhein-Westfalen, Deutschland");

		HttpGet get = new HttpGet(builder.build());
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		
		CloseableHttpResponse response = httpclient.execute(get);    
		
		String rawResult=null;
		
		try {
			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				System.out.printf("Bad response status code:%d%n", response.getStatusLine().getStatusCode());
				
			}
			
			HttpEntity entity = response.getEntity();
		    if (entity != null) {
		    	rawResult=EntityUtils.toString(entity, Charset.forName("utf-8"));
		    	System.out.printf("Result data:%n%s%n", rawResult);
		    }
		    
		} finally {
			response.close();
			
		}
		return(rawResult);
	}
	public static JSONObject retrieveWeatherForecastAsJson() throws Exception {
		//set up the end point
		URIBuilder builder = new URIBuilder("https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/weatherdata/forecast");
		
		builder.setParameter("aggregateHours", "24")
			.setParameter("contentType", "json")
			.setParameter("unitGroup", "metric")
			.setParameter("locationMode", "single")
			.setParameter("key", "BUFFZ493AMSLG33VHFPQ7QRPF")
			.setParameter("locations", "Rheinbach, Nordrhein-Westfalen, Deutschland");

		HttpGet get = new HttpGet(builder.build());
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		
		CloseableHttpResponse response = httpclient.execute(get);    
		JSONObject jsonWeatherForecast = null;
		try {
			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				System.out.printf("Bad response status code:%d%n", response.getStatusLine().getStatusCode());
				
			}
			
			HttpEntity entity = response.getEntity();
		    if (entity != null) {
		    	String rawResult=EntityUtils.toString(entity, Charset.forName("utf-8"));
		    	
		    	jsonWeatherForecast = new JSONObject(rawResult);
		    	
		    }
		    
		    
		} finally {
			response.close();
		}
		return(jsonWeatherForecast);

		
		
	}

}
