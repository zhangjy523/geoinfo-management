package operations;

import gim.config.GIMConfig;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFactory;
import org.json.JSONObject;

import com.github.andrewoma.dexx.collection.Vector;

import java.io.*;
import java.sql.SQLException;

import javax.swing.table.DefaultTableModel;

public class GIMQuery {

    public static JSONObject sendQuery(String sparqlQuery, GIMConfig gimConfig) {

        // connect to Fuseki
        RDFConnection conn = RDFConnectionFactory.connect(
                "http://" + gimConfig.getRemoteHost() + ":" + gimConfig.getLocalRDFPort() + "/ds/");

        System.out.println("http://" + gimConfig.getRemoteHost() + ":" + gimConfig.getLocalRDFPort() + "/ds/");

        // send query and get result
        QueryExecution qExec = conn.query(sparqlQuery) ;
        ResultSet rs = qExec.execSelect() ;

        // write to a ByteArrayOutputStream
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        ResultSetFormatter.outputAsJSON(outputStream, rs);

        // turn ByteArrayOutputStream into a String
        String json = new String(outputStream.toByteArray());
        JSONObject queryoutput = new JSONObject(json);

        System.out.println("json = " + json);

        qExec.close() ;
        conn.close() ;
        
        return queryoutput;

    }
    
    public static String getResultSet(String sparqlQuery, GIMConfig gimConfig)
            
    {

    	// connect to Fuseki
        RDFConnection conn = RDFConnectionFactory.connect(
                "http://" + gimConfig.getRemoteHost() + ":" + gimConfig.getLocalRDFPort() + "/ds/");

        System.out.println("http://" + gimConfig.getRemoteHost() + ":" + gimConfig.getLocalRDFPort() + "/ds/");

        // send query and get result
        QueryExecution qExec = conn.query(sparqlQuery) ;
        ResultSet rs = qExec.execSelect() ;
        
        
        

        // turn ByteArrayOutputStream into a String
        String json = ResultSetFormatter.asText(rs);
    	return json;
    }

    

}
