
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;

import org.jdom2.Element;
import org.json.JSONObject;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.CoordinateXY;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.postgresql.ds.PGSimpleDataSource;

import geometry.Envelope;
import io.structures.Feature;
import viewer.base.Layer;
import viewer.base.ListLayer;
import viewer.base.Map;
import viewer.base.MapPanel;
import viewer.base.Transformation;
import viewer.symbols.BasicSymbolFactory;
import viewer.symbols.PointSymbol;
import viewer.symbols.Symbol;

/**
 * 
 */

/**

 * @author Jingyi
 *
 */

public class MapExtension extends Map  implements MouseWheelListener{
	static String hostname = "131.220.71.164";
	static String port = "5432";
	static String dbname = "dm";
	static String user = "dm";
	static String password = "wise2019";
	static ArrayList<List<HashMap<String, String>>> listTable;
	static List<String> listGml = new ArrayList<>();
	private String fieldIdSelected;
	static JPanel panel2 = new JPanel();
	
	
	
	public String getFieldIdSelected() {
		return fieldIdSelected;
	}


	public void setFieldIdSelected(String fieldIdSelected) {
		this.fieldIdSelected = fieldIdSelected;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MapPanelExtension myMapFrame;
	private TreeMap<Integer, Layer> layers = new TreeMap<Integer, Layer>();

	private double xMin;
	private double yMin;
	private double xMax;
	private double yMax;
	private boolean autoFitToDisplay;
	
	/**
	 * The column of the mouse cursor when the mouse was pressed the last time.
	 */
	private int mouseColumn;

	/**
	 * The row of the mouse cursor when the mouse was pressed the last time.
	 */
	private int mouseRow;

	/**
	 * If frameRation {@literal >} 0, a margin is left between the map and the frame
	 * of the display.
	 */
	private double frameRatio;
	private Transformation myTransformation;
	
	
	public MapExtension(MapPanelExtension m) {
		super(m);
		myMapFrame = m;
		autoFitToDisplay = true;
		myTransformation = new Transformation();
		layers = new TreeMap<Integer, Layer>();

		xMin = Double.POSITIVE_INFINITY;
		yMin = Double.POSITIVE_INFINITY;
		xMax = Double.NEGATIVE_INFINITY;
		yMax = Double.NEGATIVE_INFINITY;

		

		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent evt) {
				if (autoFitToDisplay)
					fitMapToDisplay();
			}
		});

		//addMouseListener(this);
		addMouseMotionListener(this);
		addMouseWheelListener(this);
		
		// TODO Auto-generated constructor stub
		
		// connect to database
		PGSimpleDataSource conn = new PGSimpleDataSource();
		conn.setUrl("jdbc:postgresql://" + hostname + ":" + port + "/" + dbname);
		conn.setUser(user);
		conn.setPassword(password);

		listTable = WFSLayer.readFromDB(conn);
	}


	@Override
	public void mouseClicked(MouseEvent e) {
		/*int row = e.getY();
		int column = e.getX();
		double xCenter = myTransformation.getX(column);
		double yCenter = myTransformation.getY(row);
		listGml = WFSLayer.cursorinPolygon(xCenter, yCenter);
		String fieldId = "";
		if(listGml.size()>0) {
			fieldId = listGml.get(0);
		}
		//highlight
		ListLayer selectedLayer = new ListLayer(new BasicSymbolFactory(Color.BLACK, Color.YELLOW));
		ArrayList<Polygon> plys= WFSLayer.pointInPolygon(xCenter, yCenter);
		for (var p : plys) {
			Geometry geom = p;
			Feature feature = new Feature(geom);
			selectedLayer.add(feature);
		}
		
		myMapFrame.getMap().addLayer(selectedLayer, 2);
	
			
		
		
		//System.out.print("data from gml: field_id: "+fieldId+"\n");
		
		List<HashMap<String, String>> tableSpecies = listTable.get(0);//species
		List<HashMap<String, String>> tableTreat = listTable.get(1);//treatment
		
		String subSpecies = null;
		String plant = null;
		String year = null;
		
		for(var row1:tableSpecies) {
			String temp = (String) row1.get("field_id");
			if(temp.matches(fieldId)) {
				//System.out.print("found! data from database: field_id: "+temp+"\n");
				subSpecies = row1.get("subspecies");
				plant = row1.get("plant");
				year = row1.get("year");
			}		
			else {
				//System.out.print("not found! data from database: field_id: "+temp+"\n");
			}
			
		}
		JOptionPane.showMessageDialog(myMapFrame,"Field ID: "+fieldId +"\n"+"subspecies: "+subSpecies+"\n" +"plant: "+plant+"\n" + "year: "+year+"\n","info_species", JOptionPane.PLAIN_MESSAGE);
		
		String[][] V = new String[59][];
		String[] currentValues = new String[4];
		String[] header = {"field_id", "plant", "year", "subspecies"};
		int indexTable = 0;
		for(var row1:tableSpecies) {
			
			currentValues[0] = (String) row1.get("field_id");
			currentValues[1] = (String)row1.get("plant");
			currentValues[2] = (String)row1.get("year");
			currentValues[3] = (String)row1.get("subspecies");
			V[indexTable] = currentValues;
			indexTable++;
		}
		// Adding labels to subpanels
		JTable table = new JTable(V, header);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		//panel1.setLayout(new BorderLayout());
		//panel1.add( new JScrollPane(table), BorderLayout.CENTER );
		
		
		
		
		String treatment_type = null;
		String treatment = null;
		String date = null;
		List<Treat> treatments = new ArrayList<Treat>();
		for(var row1:tableTreat) {
			String temp = (String) row1.get("field_id");
			if(temp!=null) {
				if(temp.matches(fieldId)) {
					//System.out.print("found! data from database: field_id: "+temp+"\n");
					Treat treat = new Treat();
					treatment = row1.get("treatment");
					treatment_type = row1.get("treatment_type");
					date = row1.get("date");
					treat.setDate(date);
					treat.setField_id(temp);
					treat.setTreatment(treatment);
					treat.setTreatment_type(treatment_type);
					treatments.add(treat);
				}
				
			}		
			else {
				//System.out.print("not found! data from database: field_id: "+temp+"\n");
			}
			
		}
		int sizeTreat = treatments.size();
		
		String str = "";
		String str1 = "Field ID: "+fieldId +"\n";
		
		for(int i=0;i<sizeTreat;i++) {
			str += "treatment_type: "+ treatments.get(i).getTreatment_type()+"\n" +"treatment: "+treatments.get(i).getTreatment()+"\n" + "date: "+treatments.get(i).getDate()+"\n";
		}
		str = str1+str;
		JOptionPane.showMessageDialog(myMapFrame,str, "info_treatments",JOptionPane.PLAIN_MESSAGE);
	*/
		
	
	
			 //CDialog::Create();
			 //JTextArea.setToolTipTex();
			 /*JFrame frame = new JFrame("field info");
			 frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			 Dimension size = new Dimension(200, 200);
				frame.setSize(size);
				frame.setPreferredSize(size);
				
            try 
            {
               UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (Exception except) {
               except.printStackTrace();
            }
            MapPanelExtension panel = new MapPanelExtension();
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            panel.setOpaque(true);
            JLabel xLabel = new JLabel();
            JLabel yLabel = new JLabel();
            panel.add(xLabel);
    		panel.add(yLabel);
    		panel.setBorder(BorderFactory.createTitledBorder(new EtchedBorder(), "Point Coordinates", 0, 0));
			panel.setXYLabelText(myTransformation.getX(c), myTransformation.getY(r));
    		frame.add(panel);
    		frame.setVisible(true);*/
//				JLabel fieldid = new JLabel();
//				JLabel soiltype = new JLabel();
//				JPanel panel1 = new JPanel();
//				//panel1.setBorder(BorderFactory.createLineBorder(Color.black, 1));
//				panel1.setPreferredSize(new Dimension(300,300));
//				BoxLayout boxlayout = new BoxLayout(panel1,BoxLayout.X_AXIS);
//				panel1.setLayout(boxlayout);
//				fieldid.setText("Field ID: " + fieldId);
//				soiltype.setText("Soil Type: " + Soiltype);
//				JFrame frame = new JFrame("Field Info");
//				Dimension size = new Dimension(300,300);
//				panel1.add(fieldid);
//				panel1.add(soiltype);
//				frame.setSize(size);
//				
//				//frame.setPreferredSize(size);
//				frame.setLayout(new BorderLayout(1,1));//set a border of 5*5 between the panels
//				frame.add(panel1,BorderLayout.CENTER);
//				frame.setLocationRelativeTo(null);
//				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//				
				//mainpanel.getMap().fitMapToDisplay();
	
				//frame.add(subPanel1);
				//MapExtension mapEx = new MapExtension(panel);
				
				

				//frame.setVisible(true);
		
		
	}
	/**
	 * 
	* @Title: mouseWheelMoved
	* @Description: use the mouse wheel to control zoom in and zoom out
	* @author: Jingyi
	* @param @param e
	* @param e void
	* @version
	 */
	public void mouseWheelMoved(MouseWheelEvent e) {
		int row = e.getY();
		int column = e.getX();
		double xCenter = myTransformation.getX(column);
		double yCenter = myTransformation.getY(row);
		//System.out.print("mouseWheel");
		//if (e.isControlDown()) {
			if(e.getWheelRotation() < 0) {//zoom in
				//System.out.println("mouse wheel Up");
				myTransformation.setM(2.0 * myTransformation.getM());
			}else {//zoom out
				//System.out.println("mouse wheel Down");
				myTransformation.setM(0.5 * myTransformation.getM());
			}
		//}
		//else {
			//getParent().dispatchEvent(e);
		//}
			
			int rowTemp = myTransformation.getRow(yCenter);
			int columnTemp = myTransformation.getColumn(xCenter);
			myTransformation.setColumnOrigin(myTransformation.getColumnOrigin() + column - columnTemp);
			myTransformation.setRowOrigin(myTransformation.getRowOrigin() + row - rowTemp);
			repaint();
	}
	
	
	
	/**
	 * Defines whether the map content is automatically scaled to fit this display
	 * frame when its size is changed.
	 * 
	 * @param b if true the map content is automatically scaled
	 */
	public void setAutoFitToDisplay(boolean b) {
		autoFitToDisplay = b;
	}

	/**
	 * Adds a new layer to this map.
	 * 
	 * @param l: the layer
	 * @param i: the level to which this layer is added. Lower levels are drawn
	 *           first.
	 */
	public void addLayer(Layer l, int i) {
		this.addLayer(l, i, true);
	}

	/**
	 * Adds a new layer to this map.
	 * 
	 * @param l:               the layer
	 * @param i:               the level to which this layer is added. Lower levels
	 *                         are drawn first.
	 * @param fitMapToDisplay: whether map shall be fit to display
	 */
	public void addLayer(Layer l, int i, boolean fitMapToDisplay) {
		if (l.getExtent() == null)
			return;
		Envelope env = l.getExtent();
		xMin = Math.min(xMin, env.getxMin());
		xMax = Math.max(xMax, env.getxMax());
		yMin = Math.min(yMin, env.getyMin());
		yMax = Math.max(yMax, env.getyMax());
		if (fitMapToDisplay) {
			fitMapToDisplay();
		}

		layers.put(i, l);
	}

	public void removeLayer(int i) {
		if (layers.containsKey(i))
			layers.remove(i);
	}

	/**
	 * Redefines the transformation that transforms map coordinates to image
	 * coordinates.
	 * 
	 * @param t the new transformation
	 */
	public void setTransformation(Transformation t) {
		myTransformation = t;
	}

	/**
	 * Returns the transformation that transforms map coordinates to image
	 * coordinates.
	 * 
	 * @return the transformation
	 */
	public Transformation getTransformation() {
		return myTransformation;
	}

	/**
	 * Defines the margin between the map and the frame of this map display.
	 * 
	 * @param ratio the margin relative to the size of the frame.
	 */
	public void setFrameRatio(double ratio) {
		frameRatio = ratio;
	}

	/**
	 * Draws all Symbols
	 */
	@Override
	public void paint(Graphics gSimple) {
		Graphics2D g = (Graphics2D) gSimple;

        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHints(rh);

        double x1 = myTransformation.getX(0);
        double y1 = myTransformation.getY(this.getHeight());
        double x2 = myTransformation.getX(this.getWidth());
        double y2 = myTransformation.getY(0);
        Envelope e = new Envelope(x1, x2, y1, y2);
        Color oldColor = g.getColor();
        g.clearRect(0, 0, this.getWidth(), this.getHeight());
        for (Layer l : layers.values()) {
            for (Object o : l.query(e)) {
                Symbol mo = (Symbol) o;
                if (mo instanceof PointSymbol) {
                    Point myPoint = (Point) mo.getFeature().getGeometry();
                    int x= myTransformation.getColumn(myPoint.getX());
                    int y = myTransformation.getRow(myPoint.getY());
                    String text = mo.getFeature().getAttribute("field_id").toString();
                    g.drawString(text, x, y);
                } else {
                    mo.draw(g, myTransformation);
                }

            }
        }
        g.setColor(oldColor);
	}

	public void paintAllObjects(Graphics gSimple) {
		Graphics2D g = (Graphics2D) gSimple;

		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHints(rh);

		Envelope e = new Envelope();
		Color oldColor = g.getColor();
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		for (Layer l : layers.values()) {
			for (Object o : l.query(e)) {
				Symbol mo = (Symbol) o;
				mo.draw(g, myTransformation);
			}
		}
		g.setColor(oldColor);
	}

	/**
	 * Fits the map to this MapDisplay.
	 */
	public void fitMapToDisplay() {
		fitBoxToDisplay(xMin, yMin, xMax, yMax);
	}

	/**
	 * Fits a part of the map to the current extent of the display, specified by the
	 * part's coordinate bounds.
	 * 
	 * @param xMinBox the minimum x coordinate
	 * @param yMinBox the minimum y coordinate
	 * @param xMaxBox the maximum x coordinate
	 * @param yMaxBox the maximum y coordinate
	 */
	public void fitBoxToDisplay(double xMinBox, double yMinBox, double xMaxBox, double yMaxBox) {
		double dx = frameRatio * (xMaxBox - xMinBox);
		double dy = frameRatio * (yMaxBox - yMinBox);
		xMinBox = xMinBox - dx;
		xMaxBox = xMaxBox + dx;
		yMinBox = yMinBox - dy;
		yMaxBox = yMaxBox + dy;

		int mapWidth = this.getSize().width;
		int mapHeight = this.getSize().height;

		if (xMaxBox == xMinBox && yMaxBox == yMinBox) {
			xMaxBox += 10.0;
			yMaxBox += 10.0;
			xMinBox -= 10.0;
			yMinBox -= 10.0;
		} else if (xMaxBox == xMinBox) {
			xMaxBox += 0.01 * (yMaxBox - yMinBox);
			xMinBox -= 0.01 * (yMaxBox - yMinBox);
		} else if (yMaxBox == yMinBox) {
			yMaxBox += 0.01 * (xMaxBox - xMinBox);
			yMinBox -= 0.01 * (xMaxBox - xMinBox);
		}

		double m1 = mapWidth / (xMaxBox - xMinBox);
		double m2 = mapHeight / (yMaxBox - yMinBox);
		double m;
		int frameSizeX = 0;
		int frameSizeY = 0;

		if (m1 < m2) {
			m = m1;
			frameSizeY = (int) (0.5 * (mapHeight - m * (yMaxBox - yMinBox)));
		} else {
			m = m2;
			frameSizeX = (int) (0.5 * (mapWidth - m * (xMaxBox - xMinBox)));
		}
		int ColumnOrigin = frameSizeX - (int) (m * xMinBox);
		int RowOrigin = frameSizeY + (int) (m * yMaxBox);

		myTransformation = new Transformation(m, ColumnOrigin, RowOrigin);
		repaint();
	}

	/**
	 * Fits a part of the map to the current extent of the display, specified by the
	 * part's coordinate bounds.
	 * 
	 * @param e coordinate bounds
	 */
	public void fitBoxToDisplay(Envelope e) {
		fitBoxToDisplay(e.getxMin(), e.getyMin(), e.getxMax(), e.getyMax());
	}
	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		mouseColumn = e.getX();
		mouseRow = e.getY();
		double xCenter = myTransformation.getX(mouseColumn);
		double yCenter = myTransformation.getY(mouseRow);
		
		
		 
		
		//System.out.print(myTransformation.getX(c));
		//System.out.print(myTransformation.getX(r));
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		myTransformation.setColumnOrigin(myTransformation.getColumnOrigin() + e.getX() - mouseColumn);
		myTransformation.setRowOrigin(myTransformation.getRowOrigin() + e.getY() - mouseRow);
		repaint();
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		myTransformation.setColumnOrigin(myTransformation.getColumnOrigin() + e.getX() - mouseColumn);
		myTransformation.setRowOrigin(myTransformation.getRowOrigin() + e.getY() - mouseRow);
		mouseColumn = e.getX();
		mouseRow = e.getY();
		repaint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		int r = e.getY();
		int c = e.getX();
		myMapFrame.setXYLabelText(myTransformation.getX(c), myTransformation.getY(r));
	}

	public void resetMouseCoordinates() {
		mouseRow = -100;
		mouseColumn = -100;
	}
	
  

}
