
public class Treat {
	private int id;
	private String field_id;
	private String date;
	private String treatment;
	private String treatment_type;
	
	public Treat() {
		
		
	}
	public Treat(int id, String field_id, String date, String treatment, String treatment_type) {
		this.id = id;
		this.field_id = field_id;
		this.date = date;
		this.treatment = treatment;
		this.treatment_type = treatment_type;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getField_id() {
		return field_id;
	}

	public void setField_id(String field_id) {
		this.field_id = field_id;
	}

	public String getTreatment() {
		return treatment;
	}

	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

	public String getTreatment_type() {
		return treatment_type;
	}

	public void setTreatment_type(String treatment_type) {
		this.treatment_type = treatment_type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	

}
