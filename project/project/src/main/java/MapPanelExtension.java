
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;

import viewer.base.Map;
import viewer.base.MapPanel;

/**
 * 
 */

/**
 * @author victor
 *
 */
public class MapPanelExtension extends MapPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6063108022820064493L;
	//private static final long serialVersionUID = 1L;
	public MapExtension mycustomMap;
	public JLabel xLabel;
	public JLabel yLabel;
	public MapPanelExtension () {

		setLayout(new BorderLayout());

		// labels showing the world coordinates of the mouse pointer
		 xLabel = new JLabel();
		 yLabel = new JLabel();
			
		 
		mycustomMap = new MapExtension(this);

		// when fitting the content to the map extend, use some empty space at the
		// boundary
		mycustomMap.setFrameRatio(0.1);

		JPanel myPanel = new JPanel();
		myPanel.setLayout(new GridLayout(2, 1));
		myPanel.add(xLabel);
		myPanel.add(yLabel);
		myPanel.setBorder(BorderFactory.createTitledBorder(new EtchedBorder(), "Map Coordinates", 0, 0));

		// some GUI attributes

		myPanel.setMinimumSize(new Dimension(200, 80));
		myPanel.setPreferredSize(new Dimension(200, 80));
		
		myPanel.addMouseListener(new myMouseListener());
		
		
		//Container contentPane = frame.getContentPane();

		//contentPane.setLayout(new FlowLayout());

		//contentPane.add(button);

		this.add(BorderLayout.NORTH, myPanel);
		this.add(BorderLayout.CENTER, mycustomMap);
	}
	
	
	 
	private class myMouseListener implements MouseListener{

       

		@Override
		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
    }

	/**
	 * setter method used to update the coordinates displayed as text
	 * 
	 * @param x: the x-coordinate that is displayed
	 * @param y: the y-coordinate that is displayed
	 */
	public void setXYLabelText(double x, double y) {
		xLabel.setText(" x = " + x);
		xLabel.repaint();
		yLabel.setText(" y = " + y);
		yLabel.repaint();
	}

	/**
	 * the map displayed in this panel
	 * 
	 * @return the map
	 */
	public MapExtension getMap() {
		return mycustomMap;
	}

}
