package gim.config;

public class GIMConfig {

    // local machine
    private int sshPort;
    private String localHost;
    private int localRDFPort;

    // server
    private String remoteHost;
    private int remoteRDFPort;

    // credentials
    private String user;
    private String privateKey;


    public String getLocalHost() {

        return localHost;

    }

    public int getLocalRDFPort() {

        return localRDFPort;

    }

    public String getRemoteHost() {

        return remoteHost;

    }

    public int getRemoteRDFPort() {

        return remoteRDFPort;

    }

    public int getSshPort() {

        return sshPort;

    }

    public String getPrivateKey() {

        return privateKey;

    }

    public String getUser() {

        return user;

    }

}